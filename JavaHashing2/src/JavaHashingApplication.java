import java.util.Scanner;
import java.security.MessageDigest;
import java.nio.file.Paths;
import java.io.FileInputStream;

public class JavaHashingApplication {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		System.out.println("Welcome to my Application ");
		System.out.println("----------------------------");
		System.out.println("1 - For MD5 Hashing ");
		System.out.println("2 - For SHA - 256 Hashing ");
		System.out.println("3 - For File Hashing ");
		System.out.println("****************************** ");
		System.out.println("****************************** ");
		System.out.println("Enter Your option? ");
		
		  Scanner input1 = new Scanner(System.in);
		  int number= input1.nextInt(); 

		  if (number ==1)
		  {MD5HashFunction();}

		  else if (number ==2)
		  {	SHA256HashFunction();}
		  else if (number ==3)
		  {FileHashing();	}		
		  else 
		  {System.out.println("Some other number was entered?!");}	  
	
	 }

	public static void MD5HashFunction() throws Exception  
	{
		System.out.println("Enter Your String you want to hash? ");
		
		Scanner input = new Scanner(System.in);
		String inputstring = input.next(); 
		System.out.println("You have Entered: "+inputstring);
	  
	    MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(inputstring.getBytes());
        
        byte byteData[] = md.digest();
      
        StringBuffer hexString = new StringBuffer();
  	    for (int i=0;i<byteData.length;i++) {
  		    String hex=Integer.toHexString( byteData[i] & 0xff );
 	     	if(hex.length()==1) hexString.append('0');
 	     	hexString.append(hex);
  	                                         }
  	   System.out.println("The MD5 Hash: " + hexString.toString());  

	}
	
	public static void SHA256HashFunction() throws Exception  
	{
	 System.out.println("Enter Your String you want to hash? ");	
     Scanner input = new Scanner(System.in);	  
     String inputstring = input.nextLine(); 

	 System.out.println("You have Entered: "+inputstring);
		  
	 MessageDigest md = MessageDigest.getInstance("SHA-256");
     md.update(inputstring.getBytes());

      byte byteData[] = md.digest();
      
      StringBuffer hexString = new StringBuffer();
  	  for (int i=0;i<byteData.length;i++) {
  		   String hex=Integer.toHexString( byteData[i] & 0xff );
 	       if(hex.length()==1) hexString.append('0');
 	     	hexString.append(hex);
  	                                      }
  	
  	System.out.println("The SHA - 256 Hash: " + hexString.toString());  
	}
	
	public static void FileHashing() throws Exception  
	{
		 System.out.println("Enter the name of the file with extension (e.g. 123.txt) you want to hash? ");	
	     Scanner input = new Scanner(System.in);	  
	     String inputstring = input.next(); 

		 System.out.println("You have Entered: "+inputstring);
			  
		 MessageDigest md = MessageDigest.getInstance("SHA-256");
		 String currentdir = Paths.get(".").toAbsolutePath().normalize().toString();
		 String filepath; 
		 filepath= currentdir+"\\"+inputstring; 
		 System.out.println("file path is: "+filepath);
		 
		 FileInputStream fis = new FileInputStream(filepath);
	        byte[] dataBytes = new byte[1024];
	        int nread = 0; 
	        while ((nread = fis.read(dataBytes)) != -1) {
	          // md is the MessageDigest instance
	          md.update(dataBytes, 0, nread);
	        };
		 
	      byte byteData[] = md.digest();
	      
	      StringBuffer hexString = new StringBuffer();
	  	  for (int i=0;i<byteData.length;i++) {
	  		   String hex=Integer.toHexString( byteData[i] & 0xff );
	 	       if(hex.length()==1) hexString.append('0');
	 	     	hexString.append(hex);
	  	                                      }
	  	System.out.println("The File Hash is: " + hexString.toString()); 
		
	}

}